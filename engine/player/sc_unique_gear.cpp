// ==========================================================================
// Dedmonwakeen's Raid DPS/TPS Simulator.
// Send questions to natehieter@gmail.com
// ==========================================================================

#include "simulationcraft.hpp"

using namespace unique_gear;

#define maintenance_check( ilvl ) static_assert( ilvl >= 90, "unique item below min level, should be deprecated." )

namespace { // UNNAMED NAMESPACE

// Prefix/Suffix map to allow shorthand consumable names, when searching for the item (for potion
// action).
std::map<item_subclass_consumable, std::pair<std::vector<std::string>, std::vector<std::string>>> __consumable_substrings = {
  { ITEM_SUBCLASS_POTION, { { "potion_of_the_", "potion_of_", "potion_" }, { "_potion" } } },
  { ITEM_SUBCLASS_FLASK,  { { "flask_of_the_", "flask_of_", "flask_" }, { "_flask" } } }
};


/**
 * Forward declarations so we can reorganize the file a bit more sanely.
 */

namespace enchants
{
  /* Legacy Enchants */
//  void executioner( special_effect_t& );
}

namespace profession
{
//  void nitro_boosts( special_effect_t& );
}

namespace item
{
  /* Misc */
//  void heartpierce( special_effect_t& );

}

namespace gem
{
//  void capacitive_primal( special_effect_t& );

}

namespace set_bonus
{
//  void t17_lfr_4pc_agimelee( special_effect_t& );

  // Generic passive stat aura adder for set bonuses
  void passive_stat_aura( special_effect_t& );
}

/**
 * Select attribute operator for buffs. Selects the attribute based on the
 * comparator given (std::greater for example), based on all defined attributes
 * that the stat buff is using. Note that this is for _ATTRIBUTES_ only. Using
 * it for any kind of stats will not work (for now).
 *
 * TODO: Generic way to get "composite" stat_e, so we can extend this class to
 * work on all Blizzard stats.
 */
template<template<typename> class CMP>
struct select_attr
{
  CMP<double> comparator;

  bool operator()( const stat_buff_t& buff ) const
  {
    // Comparing to 0 isn't exactly "correct", however the odds of an actor
    // having zero for all primary attributes is slim to none. If for some
    // reason all checked attributes are 0, the last checked attribute will be
    // the one selected. The order of stats checked is determined by the
    // stat_buff_creator add_stats() calls.
    double compare_to = 0;
    stat_e compare_stat = STAT_NONE, my_stat = STAT_NONE;

    for ( size_t i = 0, end = buff.stats.size(); i < end; i++ )
    {
      if ( this == buff.stats[ i ].check_func.target<select_attr<CMP> >() )
        my_stat = buff.stats[ i ].stat;

      attribute_e stat = static_cast<attribute_e>( buff.stats[ i ].stat );
      double val = buff.player -> get_attribute( stat );
      if ( ! compare_to || comparator( val, compare_to ) )
      {
        compare_to = val;
        compare_stat = buff.stats[ i ].stat;
      }
    }

    return compare_stat == my_stat;
  }
};

std::string suffix( const item_t* item )
{
  assert( item );
  if ( item -> slot == SLOT_OFF_HAND )
    return "_oh";
  return "";
}

std::string tokenized_name( const spell_data_t* data )
{
  std::string s = data -> name_cstr();
  util::tokenize( s );
  return s;
}
}
/*
 * Initialize a special effect, based on a spell id. Returns true if the first
 * phase initialization succeeded, false otherwise. If the spell id points to a
 * spell that our system cannot support, also sets the special effect type to
 * SPECIAL_EFFECT_NONE.
 *
 * Note that the first phase initialization simply fills up special_effect_t
 * with relevant information for non-custom special effects. Second phase of
 * the initialization (performed by unique_gear::init) will instantiate the
 * proc callback, and relevant actions/buffs, or call a custom function to
 * perform the initialization.
 */
bool unique_gear::initialize_special_effect( special_effect_t& effect,
                                             unsigned          spell_id )
{
  bool ret = true;
  player_t* p = effect.player;

  // Try to find the special effect from the custom effect database
  for ( const auto dbitem: find_special_effect_db_item( spell_id ) )
  {
    // Parse auxilary effect options before doing spell data based parsing
    if ( ! dbitem -> encoded_options.empty() )
    {
      std::string encoded_options = dbitem -> encoded_options;
      for ( size_t i = 0; i < encoded_options.length(); i++ )
        encoded_options[ i ] = std::tolower( encoded_options[ i ] );
      // Note, if the encoding parse fails (this should never ever happen),
      // we don't parse game client data either.
      if ( ! special_effect::parse_special_effect_encoding( effect, encoded_options ) )
        return false;
    }
    else if ( dbitem -> cb_obj )
    {
      // Check that the custom special effect initializer is valid. Invalid special effect
      // validators could be for example spec-specific initializers (see scoped_action_callback_t
      // and child classes derived off of it).
      if ( ! dbitem -> cb_obj -> valid( effect ) )
      {
        continue;
      }

      // Custom special effect initialization is deferred, and no parsing from spell data is done
      // automatically.
      if ( dbitem -> cb_obj )
      {
        effect.custom_init_object.push_back( dbitem -> cb_obj );
      }
    }
  }

  // Setup the driver always, though honoring any parsing performed in the first phase options.
  if ( effect.spell_id == 0 )
    effect.spell_id = spell_id;

  // Custom init found a valid initializer callback, this special effect will be initialized with it
  // later on
  if ( effect.custom_init_object.size() > 0 )
  {
    return ret;
  }

  // No custom effect found, so ensure that we have spell data for the driver
  if ( p -> find_spell( effect.spell_id ) -> id() != effect.spell_id )
  {
    if ( p -> sim -> debug )
      p -> sim -> out_debug.printf( "Player %s unable to initialize special effect in item %s, spell identifier %u not found.",
          p -> name(), effect.item ? effect.item -> name() : "unknown", effect.spell_id );
    effect.type = SPECIAL_EFFECT_NONE;
    return ret;
  }

  // For generic procs, make sure we have a PPM, RPPM or Proc Chance available,
  // otherwise there's no point in trying to proc anything
  if ( effect.type == SPECIAL_EFFECT_EQUIP && ! special_effect::usable_proc( effect ) )
    effect.type = SPECIAL_EFFECT_NONE;
  // For generic use stuff, we need to have a proper buff or action that we can generate
  else if ( effect.type == SPECIAL_EFFECT_USE &&
            effect.buff_type() == SPECIAL_EFFECT_BUFF_NONE &&
            effect.action_type() == SPECIAL_EFFECT_ACTION_NONE )
  {
    effect.type = SPECIAL_EFFECT_NONE;
  }

  return ret;
}

// Second phase initialization, creates the proc callback object for generic on-equip special
// effects, or calls the custom initialization function given in the first phase initialization.
void unique_gear::initialize_special_effect_2( special_effect_t* effect )
{
  if ( effect -> custom_init || effect -> custom_init_object.size() > 0 )
  {
    if ( effect -> custom_init )
    {
      effect -> custom_init( *effect );
    }
    else
    {
      range::for_each( effect -> custom_init_object, [ effect ]( scoped_callback_t* cb ) {
        cb -> initialize( *effect );
      } );
    }
  }
  else if ( effect -> type == SPECIAL_EFFECT_EQUIP )
  {
    // Ensure we are not accidentally initializing a generic special effect multiple times
    bool exists = effect -> player -> callbacks.has_callback( [ effect ]( const action_callback_t* cb ) {
      auto dbc_cb = dynamic_cast<const dbc_proc_callback_t*>( cb );
      if ( dbc_cb == nullptr )
      {
        return false;
      }

      // Special effects are unique, and have an 1:1 relationship with (dbc proc) callbacks. Pointer
      // comparison here is enough to ensure that no special_effect_t object gets more than one
      // dbc_proc_callback_t object.
      return &( dbc_cb -> effect ) == effect;
    } );

    if ( exists )
    {
      return;
    }

    if ( effect -> item )
    {
      new dbc_proc_callback_t( effect -> item, *effect );
    }
    else
    {
      new dbc_proc_callback_t( effect -> player, *effect );
    }
  }
}

// ==========================================================================
// unique_gear::init
// ==========================================================================

void unique_gear::init( player_t* p )
{
  if ( p -> is_pet() || p -> is_enemy() ) return;

  for ( size_t i = 0; i < p -> items.size(); i++ )
  {
    item_t& item = p -> items[ i ];

    for ( size_t j = 0; j < item.parsed.special_effects.size(); j++ )
    {
      special_effect_t* effect = item.parsed.special_effects[ j ];
      if ( p -> sim -> debug )
        p -> sim -> out_debug.printf( "Initializing item-based special effect %s", effect -> to_string().c_str() );

      initialize_special_effect_2( effect );
    }
  }

  // Generic special effects, bound to no specific item
  for ( size_t i = 0; i < p -> special_effects.size(); i++ )
  {
    special_effect_t* effect = p -> special_effects[ i ];
    if ( p -> sim -> debug )
      p -> sim -> out_debug.printf( "Initializing generic special effect %s", effect -> to_string().c_str() );

    initialize_special_effect_2( effect );
  }
}

// Figure out if a given generic buff (associated with a trinket/item) is a
// stat buff of the correct type
bool buff_has_stat( const buff_t* buff, stat_e stat )
{
  if ( ! buff )
    return false;

  // Not a stat buff
  const stat_buff_t* stat_buff = dynamic_cast< const stat_buff_t* >( buff );
  if ( ! stat_buff )
    return false;

  // At this point, if "any" was specificed, we're satisfied
  if ( stat == STAT_ALL )
    return true;

  for (auto & elem : stat_buff -> stats)
  {
    if ( elem.stat == stat )
      return true;
  }

  return false;
}

// Base class for item effect expressions, finds all the special effects in the
// listed slots
struct item_effect_base_expr_t : public expr_t
{
  std::vector<const special_effect_t*> effects;

  item_effect_base_expr_t( action_t* a, const std::vector<slot_e> slots ) :
    expr_t( "item_effect_base_expr" )
  {
    const special_effect_t* e = 0;

    for ( size_t i = 0; i < slots.size(); i++ )
    {
      e = a -> player -> items[ slots[ i ] ].special_effect( SPECIAL_EFFECT_SOURCE_NONE, SPECIAL_EFFECT_EQUIP );
      if ( e && e -> source != SPECIAL_EFFECT_SOURCE_NONE )
        effects.push_back( e );

      e = a -> player -> items[ slots[ i ] ].special_effect( SPECIAL_EFFECT_SOURCE_NONE, SPECIAL_EFFECT_USE );
      if ( e && e -> source != SPECIAL_EFFECT_SOURCE_NONE )
        effects.push_back( e );
    }
  }
};

// Base class for expression-based item expressions (such as buff, or cooldown
// expressions). Implements the behavior of expression evaluation.
struct item_effect_expr_t : public item_effect_base_expr_t
{
  std::vector<expr_t*> exprs;

  item_effect_expr_t( action_t* a, const std::vector<slot_e> slots ) :
    item_effect_base_expr_t( a, slots )
  { }

  // Evaluates automatically to the maximum value out of all expressions, may
  // not be wanted in all situations. Best case? We should allow internal
  // operators here somehow
  double evaluate() override
  {
    double result = 0;
    for (auto expr : exprs)
    {
      double r = expr -> eval();
      if ( r > result )
        result = r;
    }

    return result;
  }

  virtual ~item_effect_expr_t()
  { range::dispose( exprs ); }
};

// Buff based item expressions, creates buff expressions for the items from
// user input
struct item_buff_expr_t : public item_effect_expr_t
{
  item_buff_expr_t( action_t* a, const std::vector<slot_e> slots, stat_e s, bool stacking, const std::string& expr_str ) :
    item_effect_expr_t( a, slots )
  {
    for (auto e : effects)
    {


      buff_t* b = buff_t::find( a -> player, e -> name() );
      if ( buff_has_stat( b, s ) && ( ( ! stacking && b -> max_stack() <= 1 ) || ( stacking && b -> max_stack() > 1 ) ) )
      {
        if ( expr_t* expr_obj = buff_t::create_expression( b -> name(), a, expr_str, b ) )
          exprs.push_back( expr_obj );
      }
    }
  }
};

struct item_buff_exists_expr_t : public item_effect_expr_t
{
  double v;

  item_buff_exists_expr_t( action_t* a, const std::vector<slot_e>& slots, stat_e s ) :
    item_effect_expr_t( a, slots ), v( 0 )
  {
    for (auto e : effects)
    {


      buff_t* b = buff_t::find( a -> player, e -> name() );
      if ( buff_has_stat( b, s ) )
      {
        v = 1;
        break;
      }
    }
  }

  double evaluate() override
  { return v; }
};

// Cooldown based item expressions, creates cooldown expressions for the items
// from user input
struct item_cooldown_expr_t : public item_effect_expr_t
{
  item_cooldown_expr_t( action_t* a, const std::vector<slot_e> slots, const std::string& expr ) :
    item_effect_expr_t( a, slots )
  {
    for (auto e : effects)
    {

      if ( e -> cooldown() != timespan_t::zero() )
      {
        cooldown_t* cd = a -> player -> get_cooldown( e -> cooldown_name() );
        if ( expr_t* expr_obj = cd -> create_expression( a, expr ) )
          exprs.push_back( expr_obj );
      }
    }
  }
};

struct item_cooldown_exists_expr_t : public item_effect_expr_t
{
  double v;

  item_cooldown_exists_expr_t( action_t* a, const std::vector<slot_e>& slots ) :
    item_effect_expr_t( a, slots ), v( 0 )
  {
  }

  double evaluate() override
  { return v; }
};

/**
 * Create "trinket" expressions, or anything relating to special effects.
 *
 * Note that this method returns zero (nullptr) when it cannot create an
 * expression.  The callee (player_t::create_expression) will handle unknown
 * expression processing.
 *
 * Trinket expressions are of the form:
 * trinket[.12].(has_|)(stacking_|)proc.<stat>.<buff_expr> OR
 * trinket[.12].(has_|)cooldown.<cooldown_expr>
 */
expr_t* unique_gear::create_expression( action_t* a, const std::string& name_str )
{
  enum proc_expr_e
  {
    PROC_EXISTS,
    PROC_ENABLED
  };

  enum proc_type_e
  {
    PROC_STAT,
    PROC_STACKING_STAT,
    PROC_COOLDOWN,
  };

    int ptype_idx = 1, stat_idx = 2, expr_idx = 3;
    enum proc_expr_e pexprtype = PROC_ENABLED;
    enum proc_type_e ptype = PROC_STAT;
    stat_e stat = STAT_NONE;
    std::vector<slot_e> slots;
    bool legendary_ring = false;

    std::vector<std::string> splits = util::string_split( name_str, "." );

    if ( util::is_number( splits[1] ) )
    {
      if ( splits[1] == "1" )
      {
        slots.push_back( SLOT_TRINKET_1 );
      }
      else if ( splits[1] == "2" )
      {
        slots.push_back( SLOT_TRINKET_2 );
      }
      else
        return 0;
      ptype_idx++;

      stat_idx++;
      expr_idx++;
    }
      // No positional parameter given so check both trinkets
    else
    {
      slots.push_back( SLOT_TRINKET_1 );
      slots.push_back( SLOT_TRINKET_2 );
    }

  if ( util::str_prefix_ci( splits[ ptype_idx ], "has_" ) )
    pexprtype = PROC_EXISTS;

  if ( util::str_in_str_ci( splits[ ptype_idx ], "cooldown" ) )
  {
    ptype = PROC_COOLDOWN;
    // Cooldowns dont have stat type for now
    expr_idx--;
  }

  if ( util::str_in_str_ci( splits[ ptype_idx ], "stacking_" ) )
    ptype = PROC_STACKING_STAT;

  if ( ptype != PROC_COOLDOWN && !legendary_ring )
  {
    // Use "all stat" to indicate "any" ..
    if ( util::str_compare_ci( splits[ stat_idx ], "any" ) )
      stat = STAT_ALL;
    else
    {
      stat = util::parse_stat_type( splits[ stat_idx ] );
      if ( stat == STAT_NONE )
        return 0;
    }
  }

  if ( pexprtype == PROC_ENABLED && ptype != PROC_COOLDOWN && splits.size() >= 4 )
  {
    return new item_buff_expr_t( a, slots, stat, ptype == PROC_STACKING_STAT, splits[ expr_idx ] );
  }
  else if ( pexprtype == PROC_ENABLED && ptype == PROC_COOLDOWN && splits.size() >= 3  )
  {
    return new item_cooldown_expr_t( a, slots, splits[ expr_idx ] );
  }
  else if ( pexprtype == PROC_EXISTS )
  {
    if ( ptype != PROC_COOLDOWN )
    {
      return new item_buff_exists_expr_t( a, slots, stat );
    }
    else
    {
      return new item_cooldown_exists_expr_t( a, slots );
    }
  }

  return 0;
}

// Find a consumable of a given subtype, see data_enum.hh for type values.
// Returns 0 if not found.
const item_data_t* unique_gear::find_consumable( const dbc_t& dbc,
                                                 const std::string& name,
                                                 item_subclass_consumable type )
{
  if ( name.empty() )
  {
    return nullptr;
  }

  // Poor man's longest matching prefix!
  const item_data_t* item = dbc::find_consumable( type, dbc.ptr, [&name]( const item_data_t* i ) {
    std::string n = i -> name;
    util::tokenize( n );
    return util::str_in_str_ci( n, name );
  } );

  if ( item -> id != 0 )
    return item;

  return nullptr;
}

const item_data_t* unique_gear::find_item_by_spell( const dbc_t& dbc, unsigned spell_id )
{
  for ( const item_data_t* item = dbc::items( maybe_ptr( dbc.ptr ) ); item -> id != 0; item++ )
  {
    for ( size_t spell_idx = 0, end = sizeof_array( item -> id_spell ); spell_idx < end; spell_idx++ )
    {
      if ( item -> id_spell[ spell_idx ] == static_cast<int>( spell_id ) )
        return item;
    }
  }

  return 0;
}

namespace unique_gear
{
std::vector<special_effect_db_item_t> __special_effect_db, __fallback_effect_db;
}

namespace
{
bool cmp_dbitem( const special_effect_db_item_t& elem, unsigned id )
{ return elem.spell_id < id; }
}

unique_gear::special_effect_set_t do_find_special_effect_db_item(
    const std::vector<special_effect_db_item_t>& db, unsigned spell_id )
{
  special_effect_set_t entries;

  auto it = std::lower_bound( db.begin(), db.end(), spell_id, cmp_dbitem );

  if ( it == db.end() || it -> spell_id != spell_id )
  {
    return { };
  }

  while ( it != db.end() && it -> spell_id == spell_id )
  {
    // If there's an encoded option string, just return it straight up
    if ( ! it -> encoded_options.empty() )
    {
      return { &( *it ) };
    }

    assert( it -> cb_obj );

    // Push all callback-based initializers of the same priority into the vector
    if ( entries.size() == 0 || it -> cb_obj -> priority == entries.front() -> cb_obj -> priority )
    {
      entries.push_back( &( *it ) );
    }
    else
    {
      break;
    }

    it++;
  }

  return entries;
}

special_effect_set_t find_fallback_effect_db_item( unsigned spell_id )
{ return do_find_special_effect_db_item( __fallback_effect_db, spell_id ); }

special_effect_set_t unique_gear::find_special_effect_db_item( unsigned spell_id )
{ return do_find_special_effect_db_item( __special_effect_db, spell_id ); }

void unique_gear::add_effect( const special_effect_db_item_t& dbitem )
{
  __special_effect_db.push_back( dbitem );
  if ( dbitem.fallback )
    __fallback_effect_db.push_back( dbitem );
}

void unique_gear::register_special_effect( unsigned           spell_id,
                                           const custom_fp_t& init_callback )
{
  register_special_effect( spell_id, custom_cb_t( init_callback ) );
}

void unique_gear::register_special_effect( unsigned           spell_id,
                                           const custom_cb_t& init_callback )
{
  special_effect_db_item_t dbitem;
  dbitem.spell_id = spell_id;
  dbitem.cb_obj = new wrapper_callback_t( init_callback );

  __special_effect_db.push_back( dbitem );
}

void unique_gear::register_special_effect( unsigned spell_id, const char* encoded_str )
{
  special_effect_db_item_t dbitem;
  dbitem.spell_id = spell_id;
  dbitem.encoded_options = encoded_str;

  __special_effect_db.push_back( dbitem );
}

/**
 * Master list of special effects in Simulationcraft.
 *
 * This list currently contains custom procs and procs where game client data
 * is either incorrect (so we can override values), or incomplete (so we can
 * help the automatic creation process on the simc side).
 *
 * Each line in the array corresponds to a specific spell (a proc driver spell,
 * or an "on use" spell) in World of Warcraft. There are several sources for
 * special effects:
 * 1) Items (Use, Equip, Chance on hit)
 * 2) Enchants, and profession specific enchants
 * 3) Engineering special effects (tinkers, ranged enchants)
 * 4) Gems
 *
 * Blizzard does not discriminate between the different types, nor do we
 * anymore. Each spell can be mapped to a special effect in the simc client.
 * Each special effect is fed to a new proc callback object
 * (dbc_proc_callback_t) that handles the initialization of the proc, and in
 * generic proc cases, the initialization of the buffs/actions.
 *
 * Each entry contains three fields:
 * 1) The spell ID of the effect. You can find these from third party websites
 *    by clicking on the generated link in item tooltip.
 * 2) Currently a c-string of "additional options" given for a special effect.
 *    This includes the forementioned fixes of incorrect values, and "help" to
 *    drive the automatic special effect generation process. Case insensitive.
 * 3) A callback to a custom initialization function. The function is of the
 *    form: void custom_function_of_awesome( special_effect_t& effect,
 *                                           const item_t& item,
 *                                           const special_effect_db_item_t& dbitem )
 *    Where 'effect' is the effect being created, 'item' is the item that has
 *    the special effect, and 'dbitem' is the database entry itself.
 *
 * Now, special effect creation in this new system is currently a two phase
 * process. First, the special_effect_t struct for the special effect is filled
 * with enough information to initialize the proc (for generic procs, a driver
 * spell id is sufficient), and any options given in this list (through the
 * additional options). For custom special effects, the first phase simply
 * creates a stub special_effect_t object, and no game client data is processed
 * at this time.
 *
 * The second phase of the creation process is responsible for instantiating
 * the necessary action_callback_t object (simc procs), and whatever buffs, or
 * actions are required for the proc. This is also when custom callbacks get
 * called.
 *
 * Note: The special effect initialization process is now unified for all types
 * of special effects, we no longer discriminate between item, enchant, tinker,
 * or gem based special effects.
 *
 * Note2: Enchants, addons, and possibly gems will have a separate translation
 * table in sc_enchant.cpp that maps "user given" names of enchants
 * (enchant=dancing_steel), to in game data, so we can properly initialize the
 * correct spells here. Most of the enchants etc., are automatically
 * identified.  The table will only have the "non standard" user strings we
 * currently use, and whatever else we will use in the future.
 */
void unique_gear::register_special_effects()
{

   /**
   * Enchants
   */

  /* The Burning Crusade */
//  register_special_effect(  28093, "1PPM"                               ); /* Mongoose */

  /**
   * Gems
   */

//  register_special_effect(  39958, "0.2PPM"                             ); /* Thundering Skyfire */


  /* Generic special effects begin here */
}

void unique_gear::unregister_special_effects()
{
  for ( auto& dbitem: __special_effect_db )
    delete dbitem.cb_obj;
}

void unique_gear::register_hotfixes()
{

}

void unique_gear::register_target_data_initializers( sim_t* sim )
{
  std::vector< slot_e > trinkets;
  trinkets.push_back( SLOT_TRINKET_1 );
  trinkets.push_back( SLOT_TRINKET_2 );
}

special_effect_t* unique_gear::find_special_effect( player_t* actor, unsigned spell_id, special_effect_e type )
{
  auto it = range::find_if( actor -> special_effects, [ spell_id, type ]( const special_effect_t* e ) {
    return e -> driver() -> id() == spell_id && ( type == SPECIAL_EFFECT_NONE || type == e -> type );
  });

  if ( it != actor -> special_effects.end() )
  {
    return *it;
  }

  for ( const auto& item: actor -> items )
  {
    auto it = range::find_if( item.parsed.special_effects, [ spell_id, type ]( const special_effect_t* e ) {
      return e -> driver() -> id() == spell_id && ( type == SPECIAL_EFFECT_NONE || type == e -> type );
    });

    if ( it != item.parsed.special_effects.end() )
    {
      return *it;
    }
  }

  return nullptr;
}

// Some special effects may use fallback initializers, where the fallback initializer is called if
// the special effect is not found on the actor. Typical cases include anything relating to
// class-specific special effects, where buffs for example should be unconditionally created for the
// actor. This method is called after the normal special effect initialization process finishes on
// the actor.
void unique_gear::initialize_special_effect_fallbacks( player_t* actor )
{
  special_effect_t fallback_effect( actor );

  // Generate an unique list of fallback spell ids
  std::vector<unsigned> fallback_ids;
  range::for_each( __fallback_effect_db, [ &fallback_ids ]( const special_effect_db_item_t& elem ) {
    if ( range::find( fallback_ids, elem.spell_id ) == fallback_ids.end() )
    {
      fallback_ids.push_back( elem.spell_id );
    }
  });

  // Check all fallback ids
  for ( auto fallback_id: fallback_ids )
  {
    // Actor already has a special effect with the fallback id, so don't do anything
    if ( find_special_effect( actor, fallback_id ) )
    {
      continue;
    }

    fallback_effect.reset();
    fallback_effect.spell_id = fallback_id;
    // TODO: Is this really needed?
    fallback_effect.source = SPECIAL_EFFECT_SOURCE_FALLBACK;
    fallback_effect.type = SPECIAL_EFFECT_FALLBACK;

    // Get all registered fallback effects for the spell (fallback) id
    auto dbitems = find_fallback_effect_db_item( fallback_id );
    // .. nothing found, continue
    if ( dbitems.size() == 0 )
    {
      continue;
    }

    // For all registered fallback effects
    for ( const auto& dbitem: dbitems )
    {
      // Ensure that the fallback effect is actually valid for the special effect (actor)
      if ( ! dbitem -> cb_obj -> valid( fallback_effect ) )
      {
        continue;
      }

      fallback_effect.custom_init_object.push_back( dbitem -> cb_obj );
    }

    if ( fallback_effect.custom_init_object.size() > 0 )
    {
      actor -> special_effects.push_back( new special_effect_t( fallback_effect ) );
    }
  }
}

namespace
{
bool cmp_special_effect( const special_effect_db_item_t& a, const special_effect_db_item_t& b )
{
  if ( &a == &b )
    return false;

  if ( a.spell_id == b.spell_id )
  {
    if ( ! a.encoded_options.empty() && b.encoded_options.empty() )
    {
      return true;
    }
    else if ( a.encoded_options.empty() && ! b.encoded_options.empty() )
    {
      return false;
    }
    else if ( ! a.encoded_options.empty() && ! b.encoded_options.empty() )
    {
      return a.encoded_options < b.encoded_options;
    }

    assert( a.cb_obj && b.cb_obj );
    // Note, descending priority order
    return a.cb_obj -> priority > b.cb_obj -> priority;
  }

  return a.spell_id < b.spell_id;
}

// Very limited setup to automatically apply some spell data to actions. Currently only considers
// generic (direct damage/healing) and tick damage multipliers. This should be currently enough to
// get automatica application of various spec/class specific, label-based modifiers that have
// started surfacing in 7.1.5 onwards.
void apply_spell_labels( const spell_data_t* spell, action_t* a )
{
  const auto sim = a -> sim;

  for ( size_t i = 1, end = spell -> effect_count(); i <= end; ++i )
  {
    auto& effect = spell -> effectN( i );
    if ( effect.type() != E_APPLY_AURA || effect.subtype() != A_ADD_PCT_LABEL_MODIFIER )
    {
      continue;
    }

    if ( ! a -> data().affected_by_label( effect.misc_value2() ) )
    {
      continue;
    }

    double old_value = 0, new_value = 0;
    std::string modifier_str;
    switch ( static_cast<property_type_t>( effect.misc_value1() ) )
    {
      case P_EFFECT_1:
        old_value = a -> base_multiplier;
        modifier_str = "direct/periodic damage/healing";
        a -> base_multiplier *= 1.0 + effect.percent();
        new_value = a -> base_multiplier;
        break;
      case P_GENERIC:
        old_value = a -> base_dd_multiplier;
        modifier_str = "direct damage/healing";
        a -> base_dd_multiplier *= 1.0 + effect.percent();
        new_value = a -> base_dd_multiplier;
        break;
      case P_TICK_DAMAGE:
        old_value = a -> base_td_multiplier;
        modifier_str = "periodic damage/healing";
        a -> base_td_multiplier *= 1.0 + effect.percent();
        new_value = a -> base_td_multiplier;
        break;
      default:
        break;
    }

    if ( sim -> debug && ! modifier_str.empty() )
    {
      sim -> out_debug.printf( "%s applying %s modifier from %s (id=%u) to %s (id=%u action=%s), value=%.5f -> %.5f",
        a -> player -> name(),
        modifier_str.c_str(),
        spell -> name_cstr(),
        spell -> id(),
        a -> data().name_cstr(),
        a -> data().id(),
        a -> name(),
        old_value,
        new_value );
    }
  }
}
} // unnamed namespace ends

void unique_gear::sort_special_effects()
{
  std::sort( __special_effect_db.begin(), __special_effect_db.end(), cmp_special_effect );
  std::sort( __fallback_effect_db.begin(), __fallback_effect_db.end(), cmp_special_effect );
}

// Apply all label-based modifiers to an action, if the associated spell data for the application ha
// any labels. Used by proc_action_t-derived spells (currently item special effects) to
// automatically apply various class passives (e.g., Shaman, Enhancement Shaman, ...) to those
// spells. System will be expanded in the future to cover a significantly larger portion of "spell
// application interactions" between different types of objects (e.g., actors and actions).
void unique_gear::apply_label_modifiers( action_t* a )
{
  if ( a -> data().label_count() == 0 )
  {
    return;
  }

  auto spells = dbc::class_passives( a -> player );
  range::for_each( spells, [ a ]( const spell_data_t* spell ) { apply_spell_labels( spell, a ); } );
}
