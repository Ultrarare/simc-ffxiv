// ==========================================================================
// Dedmonwakeen's Raid DPS/TPS Simulator.
// Send questions to natehieter@gmail.com
// ==========================================================================

#include "sc_report.hpp"

#include <iomanip>
#include <sstream>

namespace color
{
rgb::rgb() : r_( 0 ), g_( 0 ), b_( 0 ), a_( 255 )
{
}

rgb::rgb( unsigned char r, unsigned char g, unsigned char b )
  : r_( r ), g_( g ), b_( b ), a_( 255 )
{
}

rgb::rgb( double r, double g, double b )
  : r_( static_cast<unsigned char>( r * 255 ) ),
    g_( static_cast<unsigned char>( g * 255 ) ),
    b_( static_cast<unsigned char>( b * 255 ) ),
    a_( 1 )
{
}

rgb::rgb( const std::string& color ) : r_( 0 ), g_( 0 ), b_( 0 ), a_( 255 )
{
  parse_color( color );
}

rgb::rgb( const char* color ) : r_( 0 ), g_( 0 ), b_( 0 ), a_( 255 )
{
  parse_color( color );
}

std::string rgb::rgb_str() const
{
  std::stringstream s;

  s << "rgba(" << static_cast<unsigned>( r_ ) << ", "
    << static_cast<unsigned>( g_ ) << ", " << static_cast<unsigned>( b_ )
    << ", " << a_ << ")";

  return s.str();
}

std::string rgb::str() const
{
  return *this;
}

std::string rgb::hex_str() const
{
  return str().substr( 1 );
}

rgb& rgb::adjust( double v )
{
  if ( v < 0 || v > 1 )
  {
    return *this;
  }

  r_ = static_cast<unsigned char>(r_ * v);
  g_ = static_cast<unsigned char>(g_ * v);
  b_ = static_cast<unsigned char>(b_ * v);
  return *this;
}

rgb rgb::adjust( double v ) const
{
  return rgb( *this ).adjust( v );
}

rgb rgb::dark( double pct ) const
{
  return rgb( *this ).adjust( 1.0 - pct );
}

rgb rgb::light( double pct ) const
{
  return rgb( *this ).adjust( 1.0 + pct );
}

rgb rgb::opacity( double pct ) const
{
  rgb r( *this );
  r.a_ = pct;

  return r;
}

rgb& rgb::operator=( const std::string& color_str )
{
  parse_color( color_str );
  return *this;
}

rgb& rgb::operator+=( const rgb& other )
{
  if ( this == &( other ) )
  {
    return *this;
  }

  unsigned mix_r = ( r_ + other.r_ ) / 2;
  unsigned mix_g = ( g_ + other.g_ ) / 2;
  unsigned mix_b = ( b_ + other.b_ ) / 2;

  r_ = static_cast<unsigned char>( mix_r );
  g_ = static_cast<unsigned char>( mix_g );
  b_ = static_cast<unsigned char>( mix_b );

  return *this;
}

rgb rgb::operator+( const rgb& other ) const
{
  rgb new_color( *this );
  new_color += other;
  return new_color;
}

rgb::operator std::string() const
{
  std::stringstream s;
  operator<<( s, *this );
  return s.str();
}

bool rgb::parse_color( const std::string& color_str )
{
  std::stringstream i( color_str );

  if ( color_str.size() < 6 || color_str.size() > 7 )
  {
    return false;
  }

  if ( i.peek() == '#' )
  {
    i.get();
  }

  unsigned v = 0;
  i >> std::hex >> v;
  if ( i.fail() )
  {
    return false;
  }

  r_ = static_cast<unsigned char>( v / 0x10000 );
  g_ = static_cast<unsigned char>( ( v / 0x100 ) % 0x100 );
  b_ = static_cast<unsigned char>( v % 0x100 );

  return true;
}

std::ostream& operator<<( std::ostream& s, const rgb& r )
{
  s << '#';
  s << std::setfill( '0' ) << std::internal << std::uppercase << std::hex;
  s << std::setw( 2 ) << static_cast<unsigned>( r.r_ ) << std::setw( 2 )
    << static_cast<unsigned>( r.g_ ) << std::setw( 2 )
    << static_cast<unsigned>( r.b_ );
  s << std::dec;
  return s;
}

rgb class_color( player_e type )
{
  switch ( type )
  {
    case PLAYER_NONE:
      return color::GREY;
    case PLAYER_GUARDIAN:
      return color::GREY;
    case BARD:
      return color::COLOR_BARD;
    case WARRIOR:
      return color::COLOR_WARRIOR;
    case PALADIN:
      return color::COLOR_PALADIN;
    case MONK:
      return color::COLOR_MONK;
    case DRAGOON:
      return color::COLOR_DRAGOON;
    case WHITE_MAGE:
      return color::COLOR_WHITE_MAGE;
    case BLACK_MAGE:
      return color::COLOR_BLACK_MAGE;
    case SUMMONER:
      return color::COLOR_SUMMONER;
    case SCHOLAR:
      return color::COLOR_SCHOLAR;
    case NINJA:
      return color::COLOR_NINJA;
    case DARK_KNIGHT:
      return color::COLOR_DARK_KNIGHT;
    case ASTROLOGIAN:
      return color::COLOR_ASTROLOGIAN;
    case MACHINIST:
      return color::COLOR_MACHINIST;
    case SAMURAI:
      return color::COLOR_SAMURAI;
    case RED_MAGE:
      return color::COLOR_RED_MAGE;
    case ENEMY:
      return color::GREY;
    case ENEMY_ADD:
      return color::GREY;
    case HEALING_ENEMY:
      return color::GREY;
    case TMI_BOSS:
      return color::GREY;
    case TANK_DUMMY:
      return color::GREY;
    default:
      return color::GREY2;
  }
}

rgb resource_color( resource_e type )
{
  switch ( type )
  {
    case RESOURCE_HEALTH:
      return class_color( BARD );

    case RESOURCE_MP:
      return class_color( DRAGOON );

    case RESOURCE_TP:
      return class_color( MONK );

    case RESOURCE_NONE:
    default:
      return GREY2;
  }
}

rgb stat_color( stat_e type )
{
  switch ( type )
  {
    case STAT_STRENGTH:
      return COLOR_WARRIOR;
    case STAT_DEXTERITY:
      return COLOR_BARD;
    case ATTR_VITALITY:
      return COLOR_BARD;
    case ATTR_INTELLIGENCE:
      return COLOR_BARD;
    case ATTR_MIND:
      return COLOR_BARD;
    case ATTR_CRITICAL_HIT_RATE:
      return COLOR_BARD;
    case ATTR_DETERMINATION:
      return COLOR_BARD;
    case ATTR_DIRECT_HIT_RATE:
      return COLOR_BARD;
    case ATTR_DEFENSE:
      return COLOR_BARD;
    case ATTR_MAGIC_DEFENSE:
      return COLOR_BARD;
    case ATTR_SKILL_SPEED:
      return COLOR_BARD;
    case ATTR_SPELL_SPEED:
      return COLOR_BARD;
    case ATTR_TENACITY:
      return COLOR_BARD;
    case ATTR_PIETY:
      return COLOR_BARD;
    // TODO: REMOVE OLD STATS
    case STAT_ATTACK_POWER:
      return COLOR_MONK;
    case STAT_SPELL_POWER:
      return COLOR_BLACK_MAGE;
    case STAT_HASTE_RATING:
      return COLOR_DRAGOON;
    default:
      return GREY2;
  }
}

/* Blizzard shool colors:
 * http://wowprogramming.com/utils/xmlbrowser/live/AddOns/Blizzard_CombatLog/Blizzard_CombatLog.lua
 * search for: SchoolStringTable
 */
// These colors are picked to sort of line up with classes, but match the "feel"
// of the spell class' color
rgb school_color( school_e type )
{
  switch ( type )
  {
    // -- Single Schools
    case SCHOOL_NONE:
      return color::COLOR_NONE;
    case SCHOOL_PHYSICAL:
      return color::PHYSICAL;
    case SCHOOL_HOLY:
      return color::HOLY;
    case SCHOOL_FIRE:
      return color::FIRE;
    case SCHOOL_NATURE:
      return color::NATURE;
    case SCHOOL_FROST:
      return color::FROST;
    case SCHOOL_SHADOW:
      return color::SHADOW;
    case SCHOOL_ARCANE:
      return color::ARCANE;
    // -- Physical and a Magical
    case SCHOOL_FLAMESTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_FIRE );
    case SCHOOL_FROSTSTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_FROST );
    case SCHOOL_SPELLSTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_ARCANE );
    case SCHOOL_STORMSTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_NATURE );
    case SCHOOL_SHADOWSTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_SHADOW );
    case SCHOOL_HOLYSTRIKE:
      return school_color( SCHOOL_PHYSICAL ) + school_color( SCHOOL_HOLY );
    // -- Two Magical Schools
    case SCHOOL_FROSTFIRE:
      return color::FROSTFIRE;
    case SCHOOL_SPELLFIRE:
      return school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_FIRE );
    case SCHOOL_FIRESTORM:
      return school_color( SCHOOL_FIRE ) + school_color( SCHOOL_NATURE );
    case SCHOOL_SHADOWFLAME:
      return school_color( SCHOOL_SHADOW ) + school_color( SCHOOL_FIRE );
    case SCHOOL_HOLYFIRE:
      return school_color( SCHOOL_HOLY ) + school_color( SCHOOL_FIRE );
    case SCHOOL_SPELLFROST:
      return school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_FROST );
    case SCHOOL_FROSTSTORM:
      return school_color( SCHOOL_FROST ) + school_color( SCHOOL_NATURE );
    case SCHOOL_SHADOWFROST:
      return school_color( SCHOOL_SHADOW ) + school_color( SCHOOL_FROST );
    case SCHOOL_HOLYFROST:
      return school_color( SCHOOL_HOLY ) + school_color( SCHOOL_FROST );
    case SCHOOL_ASTRAL:
      return school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_NATURE );
    case SCHOOL_SPELLSHADOW:
      return school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_SHADOW );
    case SCHOOL_DIVINE:
      return school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_HOLY );
    case SCHOOL_SHADOWSTORM:
      return school_color( SCHOOL_SHADOW ) + school_color( SCHOOL_NATURE );
    case SCHOOL_HOLYSTORM:
      return school_color( SCHOOL_HOLY ) + school_color( SCHOOL_NATURE );
    case SCHOOL_SHADOWLIGHT:
      return school_color( SCHOOL_SHADOW ) + school_color( SCHOOL_HOLY );
    //-- Three or more schools
    case SCHOOL_ELEMENTAL:
      return color::ELEMENTAL;
    case SCHOOL_CHROMATIC:
      return school_color( SCHOOL_FIRE ) + school_color( SCHOOL_FROST ) +
             school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_NATURE ) +
             school_color( SCHOOL_SHADOW );
    case SCHOOL_MAGIC:
      return school_color( SCHOOL_FIRE ) + school_color( SCHOOL_FROST ) +
             school_color( SCHOOL_ARCANE ) + school_color( SCHOOL_NATURE ) +
             school_color( SCHOOL_SHADOW ) + school_color( SCHOOL_HOLY );
    case SCHOOL_CHAOS:
      return color::CHAOS;

    default:
      return GREY2;
  }
}
} /* namespace color */
