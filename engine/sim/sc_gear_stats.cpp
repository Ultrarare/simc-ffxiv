// ==========================================================================
// Dedmonwakeen's Raid DPS/TPS Simulator.
// Send questions to natehieter@gmail.com
// ==========================================================================

#include "simulationcraft.hpp"

// ==========================================================================
// Gear Stats
// ==========================================================================

// gear_stats_t::add_stat ===================================================

void gear_stats_t::add_stat( stat_e stat,
                             double      value )
{
  switch ( stat )
  {
    case STAT_NONE: break;

    case STAT_STRENGTH:  attribute[ ATTR_STRENGTH  ] += value; break;
    case STAT_DEXTERITY:  attribute[ STAT_DEXTERITY  ] += value; break;
    case STAT_VITALITY:  attribute[ STAT_VITALITY  ] += value; break;
    case STAT_INTELLIGENCE:  attribute[ STAT_INTELLIGENCE  ] += value; break;
    case STAT_MIND:  attribute[ STAT_MIND  ] += value; break;
    case STAT_CRITICAL_HIT_RATE: attribute[ STAT_CRITICAL_HIT_RATE  ] += value; break;
    case STAT_DETERMINATION: attribute[ STAT_DETERMINATION  ] += value; break;
    case STAT_DIRECT_HIT_RATE: attribute[ STAT_DIRECT_HIT_RATE  ] += value; break;
    case STAT_DEFENSE: attribute[ STAT_DEFENSE  ] += value; break;
    case STAT_MAGIC_DEFENSE: attribute[ STAT_MAGIC_DEFENSE  ] += value; break;
    case STAT_SKILL_SPEED: attribute[ STAT_SKILL_SPEED  ] += value; break;
    case STAT_SPELL_SPEED: attribute[ STAT_SPELL_SPEED  ] += value; break;
    case STAT_TENACITY: attribute[ STAT_TENACITY  ] += value; break;
    case STAT_PIETY: attribute[ STAT_PIETY ] += value; break;

    case STAT_HEALTH: resource[ RESOURCE_HEALTH ] += value; break;
    case STAT_TP: resource[ RESOURCE_TP ] += value; break;
    case STAT_MP:   resource[ RESOURCE_MP] += value; break;

    case STAT_MAX_HEALTH:
    case STAT_MAX_MP:
    case STAT_MAX_TP: break;

    case STAT_SPELL_POWER:       spell_power       += value; break;
    case STAT_ATTACK_POWER:      attack_power      += value; break;
    case STAT_HASTE_RATING:      haste_rating      += value; break;

    case STAT_WEAPON_DPS:   weapon_dps   += value; break;
    case STAT_WEAPON_OFFHAND_DPS:    weapon_offhand_dps    += value; break;

    case STAT_ALL:
      for ( attribute_e i = ATTRIBUTE_NONE; i < ATTRIBUTE_STAT_ALL_MAX; i++)
          { attribute[ i ] += value; }
      break;

    default: assert( 0 ); break;
  }
}

// gear_stats_t::set_stat ===================================================

void gear_stats_t::set_stat( stat_e stat,
                             double      value )
{
  switch ( stat )
  {
    case STAT_NONE: break;

    case STAT_STRENGTH:  attribute[ ATTR_STRENGTH  ] = value; break;
    case STAT_DEXTERITY:  attribute[ ATTR_DEXTERITY  ] = value; break;
    case STAT_VITALITY:    attribute[ ATTR_VITALITY  ] = value; break;
    case STAT_INTELLIGENCE:  attribute[ ATTR_INTELLIGENCE  ] = value; break;
    case STAT_MIND:  attribute[ ATTR_MIND  ] = value; break;
    case STAT_CRITICAL_HIT_RATE:  attribute[ ATTR_CRITICAL_HIT_RATE  ] = value; break;
    case STAT_DETERMINATION:  attribute[ ATTR_DETERMINATION  ] = value; break;
    case STAT_DIRECT_HIT_RATE:  attribute[ ATTR_DIRECT_HIT_RATE  ] = value; break;
    case STAT_DEFENSE:  attribute[ ATTR_DEFENSE  ] = value; break;
    case STAT_MAGIC_DEFENSE:  attribute[ ATTR_MAGIC_DEFENSE  ] = value; break;
    case STAT_SKILL_SPEED:  attribute[ ATTR_SKILL_SPEED  ] = value; break;
    case STAT_SPELL_SPEED:  attribute[ ATTR_SPELL_SPEED  ] = value; break;
    case STAT_TENACITY:  attribute[ ATTR_TENACITY  ] = value; break;
    case STAT_PIETY:  attribute[ ATTR_PIETY  ] = value; break;

    case STAT_HEALTH: resource[ RESOURCE_HEALTH ] = value; break;
    case STAT_MP:     resource[ RESOURCE_MP   ] = value; break;
    case STAT_TP:     resource[ RESOURCE_TP   ] = value; break;

    case STAT_MAX_HEALTH:
    case STAT_MAX_MP:
    case STAT_MAX_TP: break;

    case STAT_SPELL_POWER:       spell_power       = value; break;
    case STAT_ATTACK_POWER:             attack_power             = value; break;
    case STAT_HASTE_RATING: haste_rating = value; break;

    case STAT_WEAPON_DPS:   weapon_dps   = value; break;
    case STAT_WEAPON_OFFHAND_DPS:    weapon_offhand_dps    = value; break;

    case STAT_ALL:
      for ( attribute_e i = ATTRIBUTE_NONE; i < ATTRIBUTE_STAT_ALL_MAX; i++ )
          { attribute[ i ] = value; }
          break;

    default: assert( 0 ); break;
  }
}

// gear_stats_t::get_stat ===================================================

double gear_stats_t::get_stat( stat_e stat ) const
{
  switch ( stat )
  {
    case STAT_NONE: return default_value;

    case STAT_STRENGTH:  return attribute[ ATTR_STRENGTH  ];
    case STAT_DEXTERITY:  return attribute[ ATTR_DEXTERITY  ];
    case STAT_VITALITY:    return attribute[ ATTR_VITALITY  ];
    case STAT_INTELLIGENCE:  return attribute[ ATTR_INTELLIGENCE  ];
    case STAT_MIND:  return attribute[ ATTR_MIND  ];
    case STAT_CRITICAL_HIT_RATE:  return attribute[ ATTR_CRITICAL_HIT_RATE  ];
    case STAT_DETERMINATION:  return attribute[ ATTR_DETERMINATION  ];
    case STAT_DIRECT_HIT_RATE:  return attribute[ ATTR_DIRECT_HIT_RATE  ];
    case STAT_DEFENSE:  return attribute[ ATTR_DEFENSE  ];
    case STAT_MAGIC_DEFENSE:  return attribute[ ATTR_MAGIC_DEFENSE  ];
    case STAT_SKILL_SPEED:  return attribute[ ATTR_SKILL_SPEED  ];
    case STAT_SPELL_SPEED:  return attribute[ ATTR_SPELL_SPEED  ];
    case STAT_TENACITY:  return attribute[ ATTR_TENACITY  ];
    case STAT_PIETY:  return attribute[ ATTR_PIETY  ];

    case STAT_HEALTH: return resource[ RESOURCE_HEALTH ];
    case STAT_MP:   return resource[ RESOURCE_MP   ];
    case STAT_TP:   return resource[ RESOURCE_TP   ];

    case STAT_MAX_HEALTH:
    case STAT_MAX_MP:
    case STAT_MAX_TP: return default_value;

    case STAT_SPELL_POWER:       return spell_power;
    case STAT_ATTACK_POWER:             return attack_power;
    case STAT_HASTE_RATING: return haste_rating;

    case STAT_WEAPON_DPS:   return weapon_dps;
    case STAT_WEAPON_OFFHAND_DPS:    return weapon_offhand_dps;

    case STAT_ALL: return default_value;

    default: assert( 0 ); break;
  }
  return default_value;
}

std::string gear_stats_t::to_string()
{
  std::ostringstream s;
  for ( stat_e i = STAT_STRENGTH; i < STAT_MAX; i++ )
  {
    if ( i > 0 )
      s << " ";
    s << util::stat_type_abbrev( i ) << "=" << get_stat( i );
  }
  return s.str();
}

// gear_stats_t::stat_mod ===================================================

double gear_stats_t::stat_mod( stat_e stat )
{
  switch ( stat )
  {
    case STAT_ATTACK_POWER:      return 1.0;
    case STAT_SPELL_POWER:       return 1.0;
    default:                     return 1.0;
  }
}
