//==========================================================================
// Dedmonwakeen's DPS-DPM Simulator.
// Send questions to natehieter@gmail.com
// ==========================================================================

#include "simulationcraft.hpp"

// ==========================================================================
// TODO
// Write Bard
// ==========================================================================

namespace
{ // UNNAMED NAMESPACE

// ==========================================================================
// Hunter
// ==========================================================================

struct bard_t;

struct bard_td_t: public actor_target_data_t
{
  struct debuffs_t
  {
//    buff_t* hunters_mark;
  } debuffs;

  struct dots_t
  {
      dot_t* windbite;
      dot_t* venomous_bite;
      // dot_t* serpent_sting;
  } dots;

  bard_td_t( player_t* target, bard_t* p );
};

struct bard_t: public player_t
{
public:

  // Active
  // These are active buffs that are always there.
  //std::vector<std::pair<cooldown_t*, proc_t*>> animal_instincts_cds;
  struct actives_t
  {
    action_t*           windbite;
//    action_t*           piercing_shots;
//    action_t*           surge_of_the_stormgod;
//    ground_aoe_event_t* sentinel;
  } active;

  // Buffs
  struct buffs_t
  {
//    buff_t* aspect_of_the_wild;
  } buffs;

  // Cooldowns
  // Abilities which have an action and a cd
  struct cooldowns_t
  {
//    cooldown_t* bestial_wrath;

  } cooldowns;

  // Gains
  // Tracking for procs
  struct gains_t
  {
    // gain_t* critical_focus;
  } gains;

  // Procs
  struct procs_t
  {
   // proc_t* wild_call;

  } procs;

  // Talents
  // TODO: This should be switched to Role Skills
  struct talents_t
  {
    // tier 1
    // const spell_data_t* big_game_hunter;
  } talents;

  // Specialization Spells
  // Job Skills
  struct specs_t
  {
    // Shared
//    const spell_data_t* critical_strikes;

    // Bard
//    const spell_data_t* cobra_shot;
  } specs;

  bard_t( sim_t* sim, const std::string& name, race_e r = RACE_NONE ) :
    player_t( sim, BARD, name, r ),
    active( actives_t() ),
    buffs( buffs_t() ),
    cooldowns( cooldowns_t() ),
    gains( gains_t() ),
    procs( procs_t() ),
    talents( talents_t() ),
    specs( specs_t() )
  {
    // Cooldowns
    // This would be by standard spell data but we need to hardcode everything
    // until we have our own spelldata
    //cooldowns.bestial_wrath   = get_cooldown( "bestial_wrath" );

    base_gcd = timespan_t::from_seconds( 2.5 );

    // May reuse this later...
//    talent_points.register_validity_fn( [ this ] ( const spell_data_t* spell )
//    {
//      // Soul of the Huntmaster
//      if ( find_item( 151641 ) )
//      {
//        switch ( specialization() )
//        {
//          case bardBEAST_MASTERY:
//            return spell -> id() == 194306; // Bestial Fury
//          case bardMARKSMANSHIP:
//            return spell -> id() == 194595; // Lock and Load
//          case bardSURVIVAL:
//            return spell -> id() == 87935; // Serpent Sting
//          default:
//            return false;
//        }
//      }
//      return false;
//    });
  }

  // Character Definition
  void      init_spells() override;
  void      init_base_stats() override;
  void      create_buffs() override;
  void      init_gains() override;
  void      init_position() override;
  void      init_procs() override;
  void      init_rng() override;
  void      init_scaling() override;
  void      init_action_list() override;
  void      arise() override;
  void      reset() override;
  void      combat_begin() override;

  void      regen( timespan_t periodicity = timespan_t::from_seconds( 0.25 ) ) override;
  double    composite_attack_power_multiplier() const override;
  double    composite_melee_crit_chance() const override;
  double    composite_melee_haste() const override;
  double    composite_spell_haste() const override;
  double    composite_player_critical_damage_multiplier( const action_state_t* ) const override;
  double    composite_rating_multiplier( rating_e rating ) const override;
  double    composite_player_multiplier( school_e school ) const override;
  double    composite_player_target_multiplier( player_t* target, school_e school ) const override;
  void      invalidate_cache( cache_e ) override;
  void      create_options() override;
  action_t* create_action( const std::string& name, const std::string& options ) override;
  resource_e primary_resource() const override { return RESOURCE_TP; }
  role_e    primary_role() const override { return ROLE_ATTACK; }
  stat_e    primary_stat() const override { return STAT_DEXTERITY ; }
  std::string      create_profile( save_e = SAVE_ALL ) override;

  void      schedule_ready( timespan_t delta_time = timespan_t::zero( ), bool waiting = false ) override;
  void      moving( ) override;

  void              apl_default();
  std::string default_potion() const override;
  std::string default_flask() const override;
  std::string default_food() const override;

  std::string special_use_item_action( const std::string& item_name, const std::string& condition = std::string() ) const;

  target_specific_t<bard_td_t> target_data;

  bard_td_t* get_target_data( player_t* target ) const override
  {
    bard_td_t*& td = target_data[target];
    if ( !td ) td = new bard_td_t( target, const_cast<bard_t*>( this ) );
    return td;
  }
};

// Template for common hunter action code. See priest_action_t.
template <class Base>
struct bard_action_t: public Base
{
private:
  typedef Base ab;
public:
  typedef bard_action_t base_t;
  bool hasted_gcd;
  struct {
      // this was for static mastery/legendary buffs - but isn't being used now.
      // this may be able to be removed...

  } affected_by;

  bard_action_t( const std::string& n, bard_t* player,
                   const spell_data_t* s = spell_data_t::nil() ):
                   ab( n, player, s ),
                   hasted_gcd( false )
  {
    memset( &affected_by, 0, sizeof(affected_by) );

    ab::special = true;

//    if ( ab::data().affected_by( p() -> specs.hunter -> effectN( 3 ) ) )
//      hasted_gcd = true;

//    if ( ab::data().affected_by( p() -> specs.hunter -> effectN( 2 ) ) )
//      ab::cooldown -> hasted = true;


  }

  bard_t* p()
  {
    return static_cast<bard_t*>( ab::player );
  }
  const bard_t* p() const
  {
    return static_cast<bard_t*>( ab::player );
  }

  bard_td_t* td( player_t* t ) const
  {
    return p() -> get_target_data( t );
  }

  void init() override
  {
    ab::init();

    // Need to figure out what this is for... probably spec init
    // disable default gcd scaling from haste as we are rolling our own because of AotW
//    ab::gcd_haste = HASTE_NONE;

//    if ( ab::data().affected_by( p() -> specs.beast_mastery_hunter -> effectN( 1 ) ) )
//      ab::base_dd_multiplier *= 1.0 + p() -> specs.beast_mastery_hunter -> effectN( 1 ).percent();

//    if ( ab::data().affected_by( p() -> specs.beast_mastery_hunter -> effectN( 2 ) ) )
//      ab::base_td_multiplier *= 1.0 + p() -> specs.beast_mastery_hunter -> effectN( 2 ).percent();

//    if ( ab::data().affected_by( p() -> specs.marksmanship_hunter -> effectN( 4 ) ) )
//      ab::base_dd_multiplier *= 1.0 + p() -> specs.marksmanship_hunter -> effectN( 4 ).percent();

//    if ( ab::data().affected_by( p() -> specs.marksmanship_hunter -> effectN( 5 ) ) )
//      ab::base_td_multiplier *= 1.0 + p() -> specs.marksmanship_hunter -> effectN( 5 ).percent();

//    if ( ab::data().affected_by( p() -> specs.survival_hunter -> effectN( 1 ) ) )
//      ab::base_dd_multiplier *= 1.0 + p() -> specs.survival_hunter -> effectN( 1 ).percent();

//    if ( ab::data().affected_by( p() -> specs.survival_hunter -> effectN( 2 ) ) )
//      ab::base_td_multiplier *= 1.0 + p() -> specs.survival_hunter -> effectN( 2 ).percent();
  }

  timespan_t gcd() const override
  {
    timespan_t g = ab::gcd();

    if ( g == timespan_t::zero() )
      return g;
// Likely where paeons will go
//    if ( affected_by.aotw_gcd_reduce && p() -> buffs.aspect_of_the_wild -> check() )
//      g += p() -> specs.aspect_of_the_wild -> effectN( 3 ).time_value();

    if ( hasted_gcd )
      g *= ab::player -> cache.attack_haste();

    if ( g < ab::min_gcd )
      g = ab::min_gcd;

    return g;
  }

  void consume_resource() override
  {
    ab::consume_resource();
  }

  double action_multiplier() const override
  {
    double am = ab::action_multiplier();
    return am;
  }

  double composite_target_crit_chance( player_t* t ) const override
  {
    double cc = ab::composite_target_crit_chance( t );
    return cc;
  }

  double cost() const override
  {
    double cost = ab::cost();
    return cost;
  }

};


struct bard_ranged_attack_t: public bard_action_t < ranged_attack_t >
{
  bard_ranged_attack_t( const std::string& n, bard_t* player,
                          const spell_data_t* s = spell_data_t::nil() ):
                          base_t( n, player, s )
  {
    may_crit = may_direct = tick_may_crit = true;
  }

  void init() override
  {
    if ( player -> main_hand_weapon.group() != WEAPON_RANGED )
      background = true;

    base_t::init();
  }

  bool usable_moving() const override
  { return true; }

  void execute() override
  {
    base_t::execute();
  }

  void impact( action_state_t* s ) override
  {
    base_t::impact( s );
  }
};

struct bard_melee_attack_t: public bard_action_t < melee_attack_t >
{
  bard_melee_attack_t( const std::string& n, bard_t* p,
                          const spell_data_t* s = spell_data_t::nil() ):
                          base_t( n, p, s )
  {
    if ( p -> main_hand_weapon.type == WEAPON_NONE )
      background = true;
    potency = 100.0;
    may_crit = may_direct = tick_may_crit = true;
  }
};

struct bard_spell_t: public bard_action_t < spell_t >
{
public:
  bard_spell_t( const std::string& n, bard_t* player,
                  const spell_data_t* s = spell_data_t::nil() ):
                  base_t( n, player, s )
  {}

  void execute() override
  {
    base_t::execute();
  }
};

namespace attacks
{

// ==========================================================================
// Hunter Attacks
// ==========================================================================

// Auto Shot ================================================================

struct auto_shot_t: public bard_action_t < ranged_attack_t >
{
  bool first_shot;

  auto_shot_t( bard_t* p ): base_t( "auto_shot", p, spell_data_t::nil() ), first_shot( true )
  {
    school = SCHOOL_PHYSICAL;
    background = true;
    repeating = true;
    trigger_gcd = timespan_t::zero();
    special = false;
    may_crit = may_direct = true;

    potency = 100.0;
    range = 40.0;
    weapon = &p->main_hand_weapon;
    base_execute_time = weapon->swing_time;
  }

  void reset() override
  {
    base_t::reset();
    first_shot = true;
  }

  timespan_t execute_time() const override
  {
    if ( first_shot )
      return timespan_t::from_millis( 100 );
    return base_t::execute_time();
  }

  void execute() override
  {
    base_t::execute();
  }

  void impact( action_state_t* s ) override
  {
    base_t::impact( s );
  }

  double composite_target_crit_chance( player_t* t ) const override
  {
    double cc = base_t::composite_target_crit_chance( t );
    return cc;
  }
};

struct start_attack_t: public action_t
{
  start_attack_t( bard_t* p, const std::string& options_str ):
    action_t( ACTION_OTHER, "start_auto_shot", p )
  {
    parse_options( options_str );

    p -> main_hand_attack = new auto_shot_t( p );
    stats = p -> main_hand_attack -> stats;
    ignore_false_positive = true;

    trigger_gcd = timespan_t::zero();
  }

  void execute() override
  {
    player -> main_hand_attack -> schedule_execute();
  }

  bool ready() override
  { return ! target -> is_sleeping() && player -> main_hand_attack -> execute_event == nullptr; } // not swinging
};

struct melee_t: public bard_melee_attack_t
{
  bool first;

  melee_t( bard_t* player, const std::string &name = "auto_attack", const spell_data_t* s = spell_data_t::nil() ):
    bard_melee_attack_t( name, player, s ), first( true )
  {
    school             = SCHOOL_PHYSICAL;
    base_execute_time  = player -> main_hand_weapon.swing_time;
    weapon = &( player -> main_hand_weapon );
    background         = true;
    repeating          = true;
    special            = false;
    trigger_gcd        = timespan_t::zero();
  }

  timespan_t execute_time() const override
  {
    if ( ! player -> in_combat )
      return timespan_t::from_seconds( 0.01 );
    if ( first )
      return timespan_t::zero();
    else
      return bard_melee_attack_t::execute_time();
  }

  void execute() override
  {
    if ( first )
      first = false;
    bard_melee_attack_t::execute();
  }

  void impact( action_state_t* s ) override
  {
    bard_melee_attack_t::impact( s );
  }
};

// Auto attack =======================================================================

struct auto_attack_t: public action_t
{
  auto_attack_t( bard_t* player, const std::string& options_str ) :
    action_t( ACTION_OTHER, "auto_attack", player )
  {
    parse_options( options_str );
    player -> main_hand_attack = new melee_t( player );
    ignore_false_positive = true;
    range = 5;
    trigger_gcd = timespan_t::zero();
    potency = 100.0;
  }

  void execute() override
  {
    player -> main_hand_attack -> schedule_execute();
  }

  bool ready() override
  {
    if ( player -> is_moving() )
      return false;

    return ( player -> main_hand_attack -> execute_event == nullptr ); // not swinging
  }
};
//==============================
// Shared attacks
//==============================
struct heavy_shot_t: public bard_ranged_attack_t
{
  heavy_shot_t( bard_t* p, const std::string& options_str ):
    bard_ranged_attack_t( "heavy_shot", p, spell_data_t::nil())
  {
    parse_options( options_str );
    weapon_power_mod = 1;
    school = SCHOOL_PHYSICAL;
    weapon_multiplier = 1;
    weapon = &( player -> main_hand_weapon );
    trigger_gcd = timespan_t::from_seconds( 2.5 );
    resource_current = RESOURCE_TP;
    base_costs[ RESOURCE_TP ] = 50.0;
    potency = 150.0;
  }

  void execute() override
  {
    bard_ranged_attack_t::execute();
  }
};

struct venomous_bite_tick_t : public bard_spell_t
{
    venomous_bite_tick_t( bard_t* p ) :
      bard_spell_t( "venomous_bite_tick", p, spell_data_t::nil())
    {
      weapon = &( player -> main_hand_weapon );
      background = true;
      may_crit = true;
      potency = 40;
    }
};

struct venomous_bite_t : public bard_spell_t
{

  venomous_bite_t( bard_t* p, const std::string& options_str ) :
    bard_spell_t( "venomous_bite", p, spell_data_t::nil())
  {
    parse_options(options_str);
    weapon = &( player -> main_hand_weapon );
    school = SCHOOL_PHYSICAL;
    resource_current = RESOURCE_TP;
    base_costs[ RESOURCE_TP ] = 60.0;
    potency = 100;
    hasted_ticks = false;
    dot_duration = timespan_t::from_seconds(15.0);
    base_tick_time = timespan_t::from_seconds(3.0);

    tick_action = new venomous_bite_tick_t(p);
  }
};
struct windbite_tick_t : public bard_spell_t
{
    windbite_tick_t( bard_t* p ) :
      bard_spell_t( "windbite_tick", p, spell_data_t::nil())
    {
      weapon = &( player -> main_hand_weapon );
      background = true;
      may_crit = true;
      potency = 50;
    }
};

struct windbite_t : public bard_spell_t
{

  windbite_t( bard_t* p, const std::string& options_str ) :
    bard_spell_t( "windbite", p, spell_data_t::nil())
  {
    parse_options(options_str);
    weapon = &( player -> main_hand_weapon );
    school = SCHOOL_PHYSICAL;
    resource_current = RESOURCE_TP;
    base_costs[ RESOURCE_TP ] = 60.0;
    potency = 60;
    hasted_ticks = false;
    dot_duration = timespan_t::from_seconds(15.0);
    base_tick_time = timespan_t::from_seconds(3.0);

    tick_action = new windbite_tick_t(p);
  }
};

} // end attacks
// ==========================================================================
// Hunter Spells
// ==========================================================================

namespace spells
{

} //end spells

namespace buffs
{

} // end namespace buffs

bard_td_t::bard_td_t( player_t* target, bard_t* p ):
  actor_target_data_t( target, p ),
  dots( dots_t() )
{
  dots.windbite = target -> get_dot( "windbite", p );
  dots.venomous_bite = target -> get_dot( "venomous_bite", p );
//  dots.piercing_shots = target -> get_dot( "piercing_shots", p );
//  dots.lacerate = target -> get_dot( "lacerate", p );
//  dots.on_the_trail = target -> get_dot( "on_the_trail", p );
//  dots.a_murder_of_crows = target -> get_dot( "a_murder_of_crows", p );

//  debuffs.hunters_mark =
//    buff_creator_t( *this, "hunters_mark", p -> find_spell( 185365 ) );

//  debuffs.vulnerable =
//    buff_creator_t( *this, "vulnerability", p -> find_spell(187131) )
//      .default_value( p -> find_spell( 187131 ) -> effectN( 2 ).percent() +
//                      p -> artifacts.unerring_arrows.percent() )
//      .refresh_behavior( BUFF_REFRESH_DURATION );

//  debuffs.true_aim =
//    buff_creator_t( *this, "true_aim", p -> find_spell( 199803 ) )
//        .default_value( p -> find_spell( 199803 ) -> effectN( 1 ).percent() );

//  debuffs.mark_of_helbrine =
//    buff_creator_t( *this, "mark_of_helbrine", p -> find_spell( 213156 ) )
//        .default_value( p -> find_spell( 213154 ) -> effectN( 1 ).percent() );

//  debuffs.unseen_predators_cloak =
//    buff_creator_t( *this, "unseen_predators_cloak", p -> find_spell( 248212 ) )
//      .default_value( p -> find_spell( 248212 ) -> effectN( 1 ).percent() );

//  target -> callbacks_on_demise.push_back( std::bind( &bard_td_t::target_demise, this ) );
}

// bard_t::create_action ==================================================

action_t* bard_t::create_action( const std::string& name,
                                   const std::string& options_str )
{
  using namespace attacks;
  using namespace spells;

//  if ( name == "a_murder_of_crows"     ) return new                    moc_t( this, options_str );
//  if ( name == "aimed_shot"            ) return new             aimed_shot_t( this, options_str );
//  if ( name == "arcane_shot"           ) return new            arcane_shot_t( this, options_str );
//  if ( name == "aspect_of_the_eagle"   ) return new    aspect_of_the_eagle_t( this, options_str );
//  if ( name == "aspect_of_the_wild"    ) return new     aspect_of_the_wild_t( this, options_str );
    if ( name == "auto_attack"           ) return new            auto_attack_t( this, options_str );
    if ( name == "auto_shot"             ) return new           start_attack_t( this, options_str );
    if ( name == "heavy_shot"            ) return new             heavy_shot_t( this, options_str );
    if ( name == "windbite"              ) return new               windbite_t( this, options_str );
    if ( name == "venomous_bite"         ) return new          venomous_bite_t( this, options_str );
//  if ( name == "barrage"               ) return new                barrage_t( this, options_str );
//  if ( name == "bestial_wrath"         ) return new          bestial_wrath_t( this, options_str );
//  if ( name == "black_arrow"           ) return new            black_arrow_t( this, options_str );
//  if ( name == "bursting_shot"         ) return new          bursting_shot_t( this, options_str );
//  if ( name == "butchery"              ) return new               butchery_t( this, options_str );
//  if ( name == "caltrops"              ) return new               caltrops_t( this, options_str );
//  if ( name == "carve"                 ) return new                  carve_t( this, options_str );
//  if ( name == "chimaera_shot"         ) return new          chimaera_shot_t( this, options_str );
//  if ( name == "cobra_shot"            ) return new             cobra_shot_t( this, options_str );
//  if ( name == "counter_shot"          ) return new           counter_shot_t( this, options_str );
//  if ( name == "dire_beast"            ) return new             dire_beast_t( this, options_str );
//  if ( name == "dire_frenzy"           ) return new            dire_frenzy_t( this, options_str );
//  if ( name == "dragonsfire_grenade"   ) return new    dragonsfire_grenade_t( this, options_str );
//  if ( name == "explosive_shot"        ) return new         explosive_shot_t( this, options_str );
//  if ( name == "explosive_trap"        ) return new         explosive_trap_t( this, options_str );
//  if ( name == "freezing_trap"         ) return new          freezing_trap_t( this, options_str );
//  if ( name == "flanking_strike"       ) return new        flanking_strike_t( this, options_str );
//  if ( name == "fury_of_the_eagle"     ) return new      fury_of_the_eagle_t( this, options_str );
//  if ( name == "harpoon"               ) return new                harpoon_t( this, options_str );
//  if ( name == "kill_command"          ) return new           kill_command_t( this, options_str );
//  if ( name == "lacerate"              ) return new               lacerate_t( this, options_str );
//  if ( name == "marked_shot"           ) return new            marked_shot_t( this, options_str );
//  if ( name == "mongoose_bite"         ) return new          mongoose_bite_t( this, options_str );
//  if ( name == "multishot"             ) return new             multi_shot_t( this, options_str );
//  if ( name == "multi_shot"            ) return new             multi_shot_t( this, options_str );
//  if ( name == "muzzle"                ) return new                 muzzle_t( this, options_str );
//  if ( name == "piercing_shot"         ) return new          piercing_shot_t( this, options_str );
//  if ( name == "rangers_net"           ) return new            rangers_net_t( this, options_str );
//  if ( name == "raptor_strike"         ) return new          raptor_strike_t( this, options_str );
//  if ( name == "sentinel"              ) return new               sentinel_t( this, options_str );
//  if ( name == "sidewinders"           ) return new            sidewinders_t( this, options_str );
//  if ( name == "snake_hunter"          ) return new           snake_bard_t( this, options_str );
//  if ( name == "spitting_cobra"        ) return new         spitting_cobra_t( this, options_str );
//  if ( name == "stampede"              ) return new               stampede_t( this, options_str );
//  if ( name == "steel_trap"            ) return new             steel_trap_t( this, options_str );
//  if ( name == "summon_pet"            ) return new             summon_pet_t( this, options_str );
//  if ( name == "tar_trap"              ) return new               tar_trap_t( this, options_str );
//  if ( name == "throwing_axes"         ) return new          throwing_axes_t( this, options_str );
//  if ( name == "titans_thunder"        ) return new         titans_thunder_t( this, options_str );
//  if ( name == "trueshot"              ) return new               trueshot_t( this, options_str );
//  if ( name == "volley"                ) return new                 volley_t( this, options_str );
//  if ( name == "windburst"             ) return new              windburst_t( this, options_str );
  return player_t::create_action( name, options_str );
}

// bard_t::init_spells ====================================================

void bard_t::init_spells()
{
  player_t::init_spells();

}

// bard_t::init_base ======================================================

void bard_t::init_base_stats()
{
  player_t::init_base_stats();
}

// bard_t::init_buffs =====================================================

void bard_t::create_buffs()
{
  player_t::create_buffs();
}

// bard_t::init_gains =====================================================

void bard_t::init_gains()
{
  player_t::init_gains();
}

// bard_t::init_position ==================================================

void bard_t::init_position()
{
  player_t::init_position();


  if ( base.position == POSITION_FRONT )
  {
    base.position = POSITION_RANGED_FRONT;
    position_str = util::position_type_string( base.position );
  }
  else if ( initial.position == POSITION_BACK )
  {
    base.position = POSITION_RANGED_BACK;
    position_str = util::position_type_string( base.position );
  }


  if ( sim -> debug )
    sim -> out_debug.printf( "%s: Position adjusted to %s", name(), position_str.c_str() );
}

// bard_t::init_procs =====================================================

void bard_t::init_procs()
{
  player_t::init_procs();
}

// bard_t::init_rng =======================================================

void bard_t::init_rng()
{
  // RPPM Stuff i think
  player_t::init_rng();
}

// bard_t::init_scaling ===================================================

void bard_t::init_scaling()
{
  player_t::init_scaling();
  // disable stats here (like tenacity)
  // scaling -> disable( STAT_STRENGTH );
}

// bard_t::default_potion =================================================

std::string bard_t::default_potion() const
{
//  return ( true_level >= 100 ) ? "prolonged_power" :
//         ( true_level >= 90  ) ? "draenic_agility" :
//         ( true_level >= 85  ) ? "virmens_bite":
         return "disabled";
}

// bard_t::default_flask ==================================================

std::string bard_t::default_flask() const
{
//  return ( true_level >  100 ) ? "seventh_demon" :
//         ( true_level >= 90  ) ? "greater_draenic_agility_flask" :
//         ( true_level >= 85  ) ? "spring_blossoms" :
//         ( true_level >= 80  ) ? "winds" :
         return "disabled";
}

// bard_t::default_food ===================================================

std::string bard_t::default_food() const
{
//	std::string lvl100_food =
//		(specialization() == bard_SURVIVAL) ? "pickled_eel" : "salty_squid_roll";

//	std::string lvl110_food =
//		(specialization() == bard_MARKSMANSHIP ||
//		(specialization() == bard_BEAST_MASTERY && !talents.stomp->ok())) ? "nightborne_delicacy_platter" :
//			(specialization() == bard_BEAST_MASTERY) ? "the_hungry_magister" : "azshari_salad";

//	return (true_level >  100) ? lvl110_food :
//		(true_level >  90) ? lvl100_food :
//		(true_level == 90) ? "sea_mist_rice_noodles" :
//		(true_level >= 80) ? "seafood_magnifique_feast" :
        return "disabled";
}

// bard_t::init_actions ===================================================

void bard_t::init_action_list()
{
  if ( specialization() != BARD_JOB && main_hand_weapon.group() != WEAPON_RANGED )
  {
    sim -> errorf( "Player %s does not have a ranged weapon at the Main Hand slot.", name() );
  }

  if ( action_list_str.empty() )
  {
      clear_action_priority_lists();
      action_priority_list_t* precombat = get_action_priority_list( "precombat" );
      precombat -> add_action( "snapshot_stats", "Snapshot raid buffed stats before combat begins and pre-potting is done." );
      apl_default(); // DEFAULT

    // Default
    use_default_action_list = true;
    player_t::init_action_list();
  }
}

// Item Actions =======================================================================

std::string bard_t::special_use_item_action( const std::string& item_name, const std::string& condition ) const
{
  auto item = range::find_if( items, [ &item_name ]( const item_t& item ) {
    return item.has_special_effect( SPECIAL_EFFECT_SOURCE_ITEM, SPECIAL_EFFECT_USE ) && item.name_str == item_name;
  });
  if ( item == items.end() )
    return std::string();

  std::string action = "use_item,name=" + item -> name_str;
  if ( !condition.empty() )
    action += "," + condition;
  return action;
}


// NO Spec Combat Action Priority List ======================================

void bard_t::apl_default()
{
  action_priority_list_t* default_list = get_action_priority_list( "default" );

  default_list -> add_action( this, "Heavy Shot" );
  default_list -> add_action( this, "Windbite" );
}

// bard_t::reset ==========================================================

void bard_t::reset()
{
  player_t::reset();
}

// bard_t::combat_begin ==================================================

void bard_t::combat_begin()
{
  player_t::combat_begin();
}

// bard_t::arise ==========================================================

void bard_t::arise()
{
  player_t::arise();
}

// bard_t::composite_rating_multiplier =====================================

double bard_t::composite_rating_multiplier( rating_e rating ) const
{
  double m = player_t::composite_rating_multiplier( rating );

  return m;
}

// bard_t::regen ===========================================================

void bard_t::regen( timespan_t periodicity )
{
  player_t::regen( periodicity );
}

// bard_t::composite_attack_power_multiplier ==============================

double bard_t::composite_attack_power_multiplier() const
{
  double mult = player_t::composite_attack_power_multiplier();

  return mult;
}

// bard_t::composite_melee_crit_chance ===========================================

double bard_t::composite_melee_crit_chance() const
{
  double crit = player_t::composite_melee_crit_chance();

  return crit;
}

// bard_t::composite_melee_haste ===========================================

double bard_t::composite_melee_haste() const
{
  double h = player_t::composite_melee_haste();

  return h;
}

// bard_t::composite_spell_haste ===========================================

double bard_t::composite_spell_haste() const
{
  double h = player_t::composite_spell_haste();
  return h;
}

// bard_t::composite_player_critical_damage_multiplier ====================

double bard_t::composite_player_critical_damage_multiplier( const action_state_t* s ) const
{
  double cdm = player_t::composite_player_critical_damage_multiplier( s );
  return cdm;
}

// bard_t::composite_player_multiplier ====================================

double bard_t::composite_player_multiplier( school_e school ) const
{
  double m = player_t::composite_player_multiplier( school );
  return m;
}

// composite_player_target_multiplier ====================================

double bard_t::composite_player_target_multiplier( player_t* target, school_e school ) const
{
  double d = player_t::composite_player_target_multiplier( target, school );
  return d;
}

// bard_t::invalidate_cache ==============================================

void bard_t::invalidate_cache( cache_e c )
{
  player_t::invalidate_cache( c );
}

// bard_t::create_options =================================================

void bard_t::create_options()
{
  player_t::create_options();
}

// bard_t::create_profile =================================================

std::string bard_t::create_profile( save_e stype )
{
  std::string profile_str = player_t::create_profile( stype );
  return profile_str;
}

// bard_t::schedule_ready() =======================================================

/* Set the careful_aim buff state based on rapid fire and the enemy health. */
void bard_t::schedule_ready( timespan_t delta_time, bool waiting )
{
    player_t::schedule_ready( delta_time, waiting );
}

// bard_t::moving() =======================================================

/* Override moving() so that it doesn't suppress auto_shot and only interrupts the few shots that cannot be used while moving.
*/
void bard_t::moving()
{
  if ( ( executing && ! executing -> usable_moving() ) || ( channeling && ! channeling -> usable_moving() ) )
    player_t::interrupt();
}

/* Report Extension Class
 * Here you can define class specific report extensions/overrides
 */
class bard_report_t: public player_report_extension_t
{
public:
  bard_report_t( bard_t& player ):
    p( player )
  {
  }

  void html_customsection( report::sc_html_stream& /* os*/ ) override
  {
    (void)p;
    /*// Custom Class Section
    os << "\t\t\t\t<div class=\"player-section custom_section\">\n"
    << "\t\t\t\t\t<h3 class=\"toggle open\">Custom Section</h3>\n"
    << "\t\t\t\t\t<div class=\"toggle-content\">\n";

    os << p.name();

    os << "\t\t\t\t\t\t</div>\n" << "\t\t\t\t\t</div>\n";*/
  }
private:
  bard_t& p;
};

// BARD MODULE INTERFACE ==================================================

typedef std::function<void(bard_t&, const spell_data_t*)> special_effect_setter_t;


struct bard_module_t: public module_t
{
  bard_module_t(): module_t( BARD ) {}

  player_t* create_player( sim_t* sim, const std::string& name, race_e r = RACE_NONE ) const override
  {
    auto  p = new bard_t( sim, name, r );
    p -> report_extension = std::unique_ptr<player_report_extension_t>( new bard_report_t( *p ) );
    return p;
  }

  bool valid() const override { return true; }

  void init( player_t* ) const override
  {
  }

  void register_hotfixes() const override
  {
  }

  void combat_begin( sim_t* ) const override {}
  void combat_end( sim_t* ) const override {}
};

} // UNNAMED NAMESPACE

const module_t* module_t::bard()
{
  static bard_module_t m;
  return &m;
}
