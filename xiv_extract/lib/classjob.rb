require 'open-uri'
require 'csv'
require 'pry'

puts 'Loading ClassJob File'
file = open('https://raw.githubusercontent.com/viion/ffxiv-datamining/master/csv/ClassJob.csv')
puts 'Parsing CSV'
csv = CSV.parse(file)

File.open('./sc_ff_class_job_data.inc', 'w+') do |file|
  puts 'File Opened'
  selected_items = []

  puts 'Selecting Items'
  csv.each_with_index do |row, index|
    # Take only higher than 90 ilvl
    next if index < 3
    selected_items << row
  end
  puts "Found #{selected_items.count} items"
  file.write("#define CLASS_JOB_SIZE (#{selected_items.count})\r\n")
  file.write("static struct class_job_data_t __class_job_data[][CLASS_JOB_SIZE] = {\r\n")
  file.write("//#ID(0),Name(1),Abbreviation(2),(3),ClassJobCategory(4),,,,,,Modifier{HitPoints}(10),,Modifier{Strength}(12),Modifier{Vitality}(13),Modifier{Dexterity}(14),Modifier{Intelligence}(15),Modifier{Mind}(16),Modifier{Piety}(17),,,,,,,,,,ClassJob{Parent},Name{English},Item{StartingWeapon},,,,,LimitBreak1,LimitBreak2,LimitBreak3,,Item{SoulCrystal},,,,StartingLevel,\r\n")

  selected_items.each do |row|
    string = "{#{row[0]}, \"#{row[1]}\", \"#{row[2]}\", \"#{row[3]}\", #{row[4]}, #{row[5]}, #{row[6]}, #{row[7]}, #{row[8]}, #{row[9]}, #{row[10]}, #{row[11]}, #{row[12]}, #{row[13]}, #{row[14]}, #{row[15]}, #{row[16]}, #{row[17]}, #{row[18]}, #{row[19]}, #{row[20]}, #{row[21]}, #{row[22]}, #{row[23]}, #{row[24]}, #{row[25]}, #{row[26]}, #{row[27]}, \"#{row[28]}\", #{row[29]}, #{row[30]}, #{row[31]}, #{row[32]}, #{row[33]}, #{row[34]}, #{row[35]}, #{row[36]}, #{row[37]}, #{row[38]}, #{row[39]}, #{row[40]}, #{row[41]}, #{row[42]}, #{row[43]}},\r\n"
    string = string.gsub('False', 'false').gsub('True', 'true')
    file.write(string)
  end
  file.write('};')
end
