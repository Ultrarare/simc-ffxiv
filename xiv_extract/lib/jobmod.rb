require 'open-uri'
require 'csv'
require 'pry'

puts 'Loading ClassJob File'
file = open('https://raw.githubusercontent.com/viion/ffxiv-datamining/master/csv/ClassJob.csv')
puts 'Parsing CSV'
csv = CSV.parse(file)

File.open('./sc_ff_job_mod_data.inc', 'w+') do |file|
  puts 'File Opened'
  selected_items = []

  puts 'Selecting Items'
  csv.each_with_index do |row, index|
    # Take only higher than 90 ilvl
    next if index < 3
    selected_items << row
  end
  puts "Found #{selected_items.count + 1} items"
  file.write("#define CLASS_JOB_SIZE (#{selected_items.count + 1})\r\n")
  file.write("static job_mod_data_t __job_mod_data[CLASS_JOB_SIZE] = {\r\n")
  file.write("//#ID(0),Name(1),Abbreviation(2),(3),ClassJobCategory(4),,,,,,Modifier{HitPoints}(10),,Modifier{Strength}(12),Modifier{Vitality}(13),Modifier{Dexterity}(14),Modifier{Intelligence}(15),Modifier{Mind}(16),Modifier{Piety}(17),,,,,,,,,,ClassJob{Parent},Name{English},Item{StartingWeapon},,,,,LimitBreak1,LimitBreak2,LimitBreak3,,Item{SoulCrystal},,,,StartingLevel,\r\n")

  selected_items.each do |row|
    string = "{#{row[12]}, #{row[13]}, #{row[14]}, #{row[15]}, #{row[16]}, #{row[17]}},\r\n"
    string = string.gsub('False', 'false').gsub('True', 'true')
    file.write(string)
  end
  file.write('};')
end
