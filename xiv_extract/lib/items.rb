require 'open-uri'
require 'csv'
require 'pry'

puts 'Loading Items File'
file = open('https://raw.githubusercontent.com/viion/ffxiv-datamining/master/csv/Item.csv')
puts 'Parsing CSV'
csv = CSV.parse(file)

File.open('./sc_ff_item_data.inc', 'w+') do |file|
  puts 'File Opened'
  selected_items = []

  puts 'Selecting Items > ilvl 90 From CSV'
  csv.each_with_index do |row, index|
    # Take only higher than 90 ilvl
    next if index < 3
    selected_items << row if row[12].to_i > 90
  end
  puts "Found #{selected_items.count} items"
  file.write("#define ITEM_SIZE (#{selected_items.count})\r\n")
  file.write('static struct item_data_t __item_data[] = {\r\n')
  file.write('//{Id(0), Description(9), Name(10), Icon(11), iLvl(12), Rarity(13), Slot(18), Unique(21), CanBeHQ(27), ItemAction(30), Cooldown(32), ReqLvl(39), EqRes(31), JobUse(48), PhysicalDmg(50), MagicDmg(51), Delay(52), BlockRate(54), Block(55), PhysicalDef(56), MagicDef(57), Param0(58), ParamValue0(59), Param1(60), ParamValue1(61), Param2(62), ParamValue2(63), Param3(64), ParamValue3(65), Param4(66), ParamValue4(67), Param5(68), ParamValue5(69), ItemSpecialBonus(70), ItemSpecialBonusParam(71), ParamSpecial0(72), ParamSpecial0Value(73), ParamSpecial1(74), ParamSpecial1Value(75), ParamSpecial2(76), ParamSpecial2Value(77), ParamSpecial3(78), ParamSpecial3Value(79), ParamSpecial4(80), ParamSpecial4Value(81), ParamSpecial5(82), ParamSpecial5Value(83), MateriaSlotCount(84), IsPVP(87) }\r\n')

  selected_items.each do |row|
    string = "{#{row[0]}, \"#{row[9]}\", \"#{row[10]}\", #{row[11]}, #{row[12]}, #{row[13]}, #{row[18]}, #{row[21]}, #{row[27]}, #{row[30]}, #{row[32]}, #{row[39]}, #{row[41]}, #{row[48]}, #{row[50]}, #{row[51]}, #{row[52]}, #{row[54]}, #{row[55]}, #{row[56]}, #{row[57]}, #{row[58]}, #{row[59]}, #{row[60]}, #{row[61]}, #{row[62]}, #{row[63]}, #{row[64]}, #{row[65]}, #{row[66]}, #{row[67]}, #{row[68]}, #{row[69]}, #{row[70]}, #{row[71]}, #{row[72]}, #{row[73]}, #{row[74]}, #{row[75]}, #{row[76]}, #{row[77]}, #{row[78]}, #{row[79]}, #{row[80]}, #{row[81]}, #{row[82]}, #{row[83]}, #{row[86]}, #{row[87]},\r\n"
    string = string.gsub('False', 'false').gsub('True', 'true')
    file.write(string)
  end
  file.write('};')
end
